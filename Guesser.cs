using System;

namespace HelloWorld
{
    class Guesser
    {
        static void Main(string[] args)
        {
            string SECRET = "giraffe";

            bool guessed = false;
            int max_guesses = 3;

            while (!guessed && max_guesses > 0)
            {
                max_guesses = max_guesses - 1;
                string message = $"try again, you have {max_guesses} guesses left";

                Console.Write("what's your guess?: ");
                string guess = Console.ReadLine().ToLower();

                if (guess == SECRET)
                {
                    guessed = true;
                    message = $"you win! {SECRET} is right";
                }
                else
                {
                    if (max_guesses == 0)
                    {
                        message = $"you lost: the word was {SECRET}";
                    }
                }
                Console.WriteLine(message);
            }
        }
    }
}
