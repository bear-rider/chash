using System;

namespace HelloWorld
{
    class Greeter
    {
        static void Main(string[] args)
        {
            Console.Write("name: ");
            string name = Console.ReadLine();
            Console.Write($"hello, {name}");
        }
    }
}
