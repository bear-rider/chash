using System;

namespace HelloWorld
{
    class ItalianChef : Chef
    {
        public ItalianChef(string name) : base(name)
        {

        }

        public override void signatureDish()
        {
            Console.WriteLine($"{Name} makes some pasta");
        }
    }
    class Chef
    {
        public string Name;
        public Chef(string name)
        {
            this.Name = name;
        }
        public void boilWater()
        {
            Console.WriteLine($"{Name} puts the kettle on");
        }

        public virtual void signatureDish()
        {
            Console.WriteLine($"{Name} scrambles some eggs");
        }
    }

    public class Kitchen
    {
        public static void Main(string[] args)
        {
            Chef paul = new Chef("Paul");
            paul.boilWater();
            paul.signatureDish();

            Console.WriteLine("---");

            Chef pablo = new ItalianChef("Pablo");
            pablo.boilWater();
            pablo.signatureDish();

        }
    }
}
