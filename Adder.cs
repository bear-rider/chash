using System;

namespace HelloWorld
{
    class Adder
    {
        static void Main(string[] args)
        {
            Console.Write("first number: ");
            double first = Convert.ToDouble(Console.ReadLine());
            Console.Write("next number: ");
            double next = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"sum = {first + next}");
        }
    }
}
