using System;

namespace HelloWorld
{
    public class SongCount
    {
        public static void Main(string[] args)
        {
            Song holiday = new Song("Green Day", "Holiday");
            Console.WriteLine(Song.getCount());
            Console.WriteLine(holiday.Artist);
            Console.WriteLine(holiday.Title);
        }

    }
    class Song
    {
        public string Artist;
        public string Title;
        private static int count = 0;

        public Song(string artist, string title)
        {
            this.Title = title;
            this.Artist = artist;
            count++;
        }

        public static int getCount()
        {
            return count;
        }
    }
}
