using System;

namespace HelloWorld
{
    class Story
    {
        static void Main(string[] args)
        {
            string name = "John";
            int age = -35;

            Console.WriteLine($"There once was a man named {name}");
            Console.WriteLine($"He was {Math.Abs(age)} years old");
            Console.WriteLine($"He really liked the name {name}");
            Console.WriteLine($"But didn't like being {Math.Round(age * 1.0)}");
        }
    }
}
