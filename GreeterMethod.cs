using System;

namespace HelloWorld
{
    class GreeterMethod
    {
        static void Main(string[] args)
        {
            Console.Write("name: ");
            string name = Console.ReadLine();
            Console.WriteLine(greet(name));
        }


        static string greet(string name)
        {
            return ($"hello, {name}");
        }
    }
}
