using System;

namespace HelloWorld
{
    class Calculator
    {
        static void Main(string[] args)
        {
            Console.Write("first number: ");
            decimal first = Convert.ToDecimal(Console.ReadLine());

            Console.Write("and the operator(+ - * /): ");
            string op = Console.ReadLine();

            Console.Write("next number: ");
            decimal next = Convert.ToDecimal(Console.ReadLine());

            string result = "";

            switch (op)
            {
                case "*":
                    result = $"{first} {op} {next} is {first * next}";
                    break;

                case "+":
                    result = $"{first} {op} {next} is {first + next}";
                    break;

                case "/":
                    if (next == 0)
                    {
                        try
                        {
                            throw new Exception();
                        }
                        catch { result = "divisor cannot be 0"; }
                    }
                    else
                    {
                        result = $"{first} {op} {next} is {Math.Round((first / next), 2, MidpointRounding.ToEven)}";
                    }
                    break;

                case "-":
                    result = $"{first} {op} {next} is {(first - next)}";
                    break;

                default:
                    result = "invalid operator. allowed values: * / - + ";
                    break;

            }

            Console.WriteLine(result);
        }

    }
}
